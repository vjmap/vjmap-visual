import marker from './marker'
import symbol from './symbol'
import popup from './popup'
import line from './line'
import fill from './fill'
import fillExtrusion from './fillExtrusion'
import circle from './circle'
import heatmap from './heatmap'
import animateLine from './animateLine'
import animateSymbol from './animateSymbol'
import animateFill from './animateFill'
import text from './text'
import symbolCluster from './symbolCluster'
import markerCluster from './markerCluster'
import breathingMarker from './breathingMarker'
import rotatingMarker from './rotatingMarker'
import haloRingMarker from './haloRingMarker'
import diffusedMarker from './diffusedMarker'
import textBorderMarker from './textBorderMarker'
import fluorescenceMarker from './fluorescenceMarker'
import curve from './curve'
import lineExtrusions from './lineExtrusions'
import arrowLine from './arrowLine'
import pathAnimate from './pathAnimate'
import symbolPathAnimate from './symbolPathAnimate'
import fillExtrusionsAnimate from './fillExtrusionsAnimate'
import geojson from './geojson'
import draw from './draw'
import raster from './raster'
import vector from './vector'
import divOverlay from './divOverlay'
import svgOverlay from './svgOverlay'
import background from './background'
import sky from './sky'

export default {
    "marker": marker,
    "symbol": symbol,
    "circle": circle,
    "text": text,
    "popup": popup,
    "line": line,
    "fill": fill,
    "fillExtrusion": fillExtrusion,
    "heatmap": heatmap,
    "animateLine": animateLine,
    "animateSymbol": animateSymbol,
    "animateFill": animateFill,
    "symbolCluster": symbolCluster,
    "markerCluster": markerCluster,
    "breathingMarker": breathingMarker,
    "rotatingMarker": rotatingMarker,
    "haloRingMarker": haloRingMarker,
    "diffusedMarker": diffusedMarker,
    "textBorderMarker": textBorderMarker,
    "fluorescenceMarker": fluorescenceMarker,
    "curve": curve,
    "lineExtrusions": lineExtrusions,
    "arrowLine": arrowLine,
    "pathAnimate": pathAnimate,
    "symbolPathAnimate": symbolPathAnimate,
    "fillExtrusionsAnimate": fillExtrusionsAnimate,
    "draw": draw,
    "geojson": geojson,
    "raster": raster,
    "vector": vector,
    "background": background,
    "sky": sky,
    "divOverlay": divOverlay,
    "svgOverlay": svgOverlay,
} as any;